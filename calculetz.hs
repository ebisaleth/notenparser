module Letzbot where 

---------------------------------------------
--                                         --
-- calculetz    ::                         --
-- a reinhold letz bot                     --
--                                         --
---------------------------------------------

{-  11/08/2016 | ^`# *~*letz fetz*~* #`^
               | muelltonne is working now -}  

{-  14/08/2016 | and this was the point where
               | she realized she should have 
               | used Data.Text...         -}          

--TODO: Kommentare... Explizite Listenrekursion?? 
--TODO: SEP & BA aus der Mülltonne ausschließen 

import Data.List


data Fach = HF | NF 
  deriving (Show, Eq, Read)
  

data Modul = Modul { sem  :: Int
                   , ects :: Int
                   , note :: Float
                   , fach :: Fach
                   , name :: String }

instance Eq Modul where 
  (==) (Modul _ e0 n0 _ _) (Modul _ e1 n1 _ _) = e0 == e1 && n0 == n1

instance Ord Modul where 
  (<=) (Modul _ e0 n0 _ _) (Modul _ e1 n1 _ _) 
    | n0 == n1  = e0 <= e1 
    | otherwise = n0 <= n1 

instance Show Modul where
  show (Modul sem ects note f name) = name ++ ": (" ++(show ects) ++ "," ++ (show note) ++ ")"

-------------------------------------- 
-- testing things                   --
--------------------------------------

main :: IO ()
main = do 
  input <- readFile "/home/elisabeth/notenparser/test"
  let inputlist = wordlines input
  let modullist = concat $ fmap killMaybe $ fmap assembleModul $ inputlist 
  let nfList = filter (not.istHF) modullist 
  putStrLn $ "Module gefunden: " ++ (show (length modullist))
  putStrLn $ "ECTS Gesamt:  " ++ (show (sum (fmap ects modullist)))
  putStrLn $ "Notenschnitt vor der Mülltonne: "
  putStrLn $ show $ calculateAvg $ fmap tupelify $ filter istBenotet modullist
  putStrLn $ "Notenschnitt nach der Mülltonne: "
  putStrLn $ show $ calculateAvg $ fmap tupelify $ filter istBenotet ((filterMuell modullist) ++ nfList)
  putStrLn $ "Gewertete Module: "
  putStrLn $ show $ length $ filter istBenotet $ filterMuell modullist ++ nfList


mainDebug :: IO ()
mainDebug = do 
  input <- readFile "/home/elisabeth/notenparser/test1"
  let inputlist = wordlines input
  let modullist = concat $ fmap killMaybe $ fmap assembleModul $ inputlist 
  let nfList = filter (not.istHF) modullist 
  putStrLn $ "Module gefunden: " ++ (show (length modullist))
  putStrLn $ "ECTS Gesamt:  " ++ (show (sum (fmap ects modullist)))
  putStrLn $ "Notenschnitt vor der Mülltonne: "
  putStrLn $ show $ calculateAvg $ fmap tupelify $ filter istBenotet modullist
  putStrLn $ "Notenschnitt nach der Mülltonne: "
  putStrLn $ show $ calculateAvg $ fmap tupelify $ filter istBenotet ((filterMuell modullist) ++ nfList)
  putStrLn $ "Gewertete Module: "
  putStrLn $ show $ length $ filter istBenotet $ filterMuell modullist ++ nfList
  putStrLn $ "-------\nDebug-Output: \n--------"
  putStrLn $ "Modulliste: "
  putStrLn $ show modullist
  putStrLn $ "Sortierte umgedrehte Modulliste: "
  putStrLn $ show $ reverse $ sort modullist
  putStrLn $ "Liste nach der Mülltonne: "
  putStrLn $ show $ filterMuell modullist
  putStrLn $ "Gewertete Module: "
  putStrLn $ show $ filterMuell modullist ++ nfList

------------------------------------
-- parsing stuff                  --
------------------------------------
ffloor = realToFrac . floor
  
wordlines = fmap words . lines 


parseSem :: String -> Maybe Int
parseSem numstr = case (reads numstr :: [(Int,String)]) of 
  [(x,"")] -> if (x > 0 && x <= 99) then (Just x) else Nothing
  _        -> Nothing
  
parseECTS :: String -> Maybe Int
parseECTS numstr = case (reads numstr :: [(Int,String)]) of 
  [(x,"")] -> if (x `mod`3 == 0 && x > 0 && x <= 15) then (Just x) else Nothing
  _        -> Nothing
  

-- why does Float even exist
parseNote :: String -> Maybe Float
parseNote numstr = case (reads numstr :: [(Float,String)]) of 
  [(x,"")] -> if (x<=4.0 && x >= 0.0 && x - (ffloor x) `elem` [0.0,0.29999995,0.70000005]) then (Just x) else Nothing
  _        -> Nothing  

parseFach :: String -> Maybe Fach
parseFach fachstr = case (reads fachstr :: [(Fach,String)]) of 
  [(x,"")] -> Just x
  _        -> Nothing

-- s e n f  
assembleModul :: [String] -> Maybe Modul
assembleModul (sem:ects:note:fach:namelist) = case (parseSem sem, parseECTS ects, parseNote note,parseFach fach) of 
  (Just s, Just e, Just n, Just f) -> Just (Modul s e n f (unwords namelist))
  _                        -> Nothing
assembleModul _             = Nothing 

killMaybe :: Maybe Modul -> [Modul] 
killMaybe (Just m) = [m] 
killMaybe _        = []


-------------------------------------- 
-- calculations happen down there   --
--------------------------------------


{----- modul level -----}

istHF :: Modul -> Bool 
istHF (Modul _ _ _ f _) = f == HF   

istUnbenotet :: Modul -> Bool 
istUnbenotet (Modul _ _ m _ _) = m == 0.0

istBenotet :: Modul -> Bool 
istBenotet = not.istUnbenotet

filterMuell :: [Modul] -> [Modul]
filterMuell mods = filterMuellAcc (sortedHFBenotet) (sum (fmap ects mods) `div` 6) [] 
  where sortedHFBenotet = reverse $ sort $ filter istHF $ filter (istBenotet) mods

-- this shouldn't be a tld but who cares
filterMuellAcc :: [Modul] -> Int -> [Modul] -> [Modul] 
filterMuellAcc [] _ _ = []
filterMuellAcc mods@(worst:rest) muellSize muell
  | currentMuell + (ects worst) <= muellSize = filterMuellAcc rest muellSize (worst:muell)
  | currentMuell == muellSize                = mods
  | otherwise                                = (decreaseECTS worst (muellSize - currentMuell)):rest
    where currentMuell = (sum (fmap ects muell))

-- story of my life 
decreaseECTS :: Modul -> Int -> Modul 
decreaseECTS (Modul s e n f name) x = (Modul s (e-x) n f name) 

{----- tupel level -----}

tupelify :: Modul -> (Int,Float)
tupelify (Modul _ ects note _ _) = (ects,note)

weight :: (Int,Float) -> (Int,Float)
weight (ects,note) = (ects,((realToFrac ects)*note))

divide :: (Int,Float) -> Float 
divide (ects,note) = note/(realToFrac ects)

calculateAvg :: [(Int,Float)] -> Float
calculateAvg noten = divide $ foldl (\(x0,y0) (x1,y1) -> (x0+x1,y0+y1)) (0,0) $ fmap weight noten

---------------------------------
-- Assorted usefulness         --
---------------------------------


-- ects numbers are not dividible by three which may cause weirdness 
testlist = [(Modul 2 1 3.7 HF "TodesEiP"),(Modul 2 2 2.7 HF "EiP"),(Modul 1 4 0.0 NF "Kant Lektuerekurs"),(Modul 1 4 2.0 HF "Turing Lektürekurs"),(Modul 1 4 0.0 HF "Informatikbasteln"),(Modul 2 5 1.3 HF "Rechnerarchitektur")]

  
