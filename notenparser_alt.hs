module Modulparser where 

---------------------------------------------
--                                         --
-- notenparser    ::                       --
--                                         --
---------------------------------------------

import Data.List


data F = HF | NF 
  deriving (Show, Eq)
  
--               Semester  Note     Modulname
--                  |        |       |
data Modul = Modul Int Int Float F String -- TODO: Recordsyntax...
--                     |         |
--                   ECTS      HF/NF

type Mark = (Int,Float)

instance Show Modul where
  show (Modul sem ects mark f name) = ' ':name ++ ": " ++ (show mark)

main :: IO ()
main = do 
  input <- readFile "/home/elisabeth/Dropbox/#Uni/notenparser/test"
  let inputlist = drop 1 (wordlines input)
  --- TODO: hier sollte markList auf korrektheit geprüft werden, um fehler zu fangen. 
  let marklist = fmap makeMark $ magic inputlist 
  putStrLn $ "Module gefunden: " ++ (show (length marklist))
  putStrLn $ "Davon unbenotet: " ++ (show (length (filter istUnbenotet marklist)))
  putStrLn $ "Notenschnitt vor der Mülltonne: "
  putStrLn $ show $ calculateAvg $ fmap tupelify marklist
  putStrLn $ "Notenschnitt nach der Mülltonne: "
  let output = calculateAvg $ filterMuell marklist 
  putStrLn $ show output 
  
------------------------------------
-- parsing stuff                  --
------------------------------------


magic = filter ( \y -> (head y /= "-") && (head y /= ".")) 
  
wordlines = fmap words . lines 
  
isLegitNumber :: String -> Bool  
isLegitNumber numstr = case (reads numstr :: [(Int,String)]) of 
  [(x,"")] -> True 
  _        -> False 
  
-- bitte nur für bereits geprüfte strings aufrufen, sonst fehleralarm.  
makeMark :: [String] -> Modul
makeMark (sem:ects:mark:namelist) = (Modul (read sem :: Int) (read ects :: Int) (read mark :: Float) HF (unwords namelist))
  
  
  
  
--------------------------------------
-- calculations happen down there   --
--------------------------------------


muelltonne :: [Mark] -> [Mark]
muelltonne mlist = muelltonneAcc mlist []
--                          acc
--                           |
muelltonneAcc :: [Mark] -> [Mark] -> [Mark]
--muelltonneAcc [] _ = [] 
muelltonneAcc mlist@(m:ms) acc 
  | sum (fmap fst acc) >= div (sum ((fmap fst mlist)++(fmap fst acc))) 6 = mlist -- make it so that überstand counts
  | otherwise = muelltonneAcc (ms) (m:acc)  


istHF :: Modul -> Bool 
istHF (Modul _ _ _ f _) = f == HF   

istUnbenotet :: Modul -> Bool 
istUnbenotet (Modul _ _ m _ _) = m == 0

tupelify :: Modul -> (Int,Float)
tupelify (Modul _ ects mark _ _) = (ects,mark)

tupelcomp :: Mark -> Mark -> Ordering 
tupelcomp (e1,m1) (e2,m2) 
  | m1 > m2 || m2 > m1 = compare m1 m2 
  | otherwise          = compare e1 e2 

weight :: Mark -> Mark 
weight (ects,mark) = (ects,((realToFrac ects)*mark))

marksum :: (Eq a, Eq b, Num a, Num b) => (a,b) -> (a,b) -> (a,b) 
marksum (x1,y1) (x2,y2) 
  |y1 /= 0 && y2 /= 0 = ((x1+x2),(y1+y2))
  |y1 /= 0            = (x1,y1)
  |y2 /= 0            = (x2,y2)

divide :: Mark -> Float 
divide (ects,mark) = mark/(realToFrac ects)

filterMuell :: [Modul] -> [Mark] 
filterMuell marks = (muelltonne(reverse (sortBy tupelcomp (fmap tupelify hfnoten)))) ++ (fmap tupelify nfnoten) 
  where hfnoten = filter istHF       $ filter (not.istUnbenotet) marks 
        nfnoten = filter (not.istHF) $ filter (not.istUnbenotet) marks

calculateAvg :: [Mark] -> Float
calculateAvg marks = divide $ foldl (marksum) (0,0) $ map weight marks 


---------------------------------
-- Assorted usefulness         --
---------------------------------

splitBy :: (a -> Bool) -> [a] -> ([a],[a])
splitBy p list = splitByAcc p list ([],[])
  where splitByAcc p [] acc = acc
        splitByAcc p (x:xs) (truesies,falsies)
		  | p x       = splitByAcc p xs (x:truesies,falsies)
		  | otherwise = splitByAcc p xs (truesies,x:falsies) 


testlist = [(Modul 1 9 2.7 HF "EiP"),(Modul 1 6 0.0 NF "Kant Lektuerekurs"),(Modul 2 6 1.3 HF "Rechnerarchitektur")]
